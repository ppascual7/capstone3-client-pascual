import moment from 'moment'

export default function dateFormatter(date, format){

    return moment(date).format(format)

}