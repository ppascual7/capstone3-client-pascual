const currencyFormatter = new Intl.NumberFormat('en-US', {
	style: 'currency',
	currency: 'PHP',
  });

export default currencyFormatter