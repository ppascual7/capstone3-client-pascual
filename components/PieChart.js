import { Pie } from 'react-chartjs-2'
import React, {useState, useEffect} from 'react'
import randomcolor from 'randomcolor'

export default function PieChart({data, incomeOverExpense}){

    const [state, setState] = useState({
        description: [],
        amount: [],
        bgColors: []
    })

    useEffect(()=>{
        let description = []
        let amount = []
        let bgColors = []

        // Check if flag for incomeOverExpense is Enabled to sum up total Income and total Expense
        if(incomeOverExpense){
            description=["Expense","Income"]
            let totalExpense = 0
            let totalIncome = 0
            data.map( each => {
                if(each.categoryType === "expense") totalExpense+=each.amount
                else totalIncome+=each.amount
            })
            amount=[totalExpense,totalIncome]
            bgColors=["orange", "blue"]

        }
        // If flag incomeOverExpense is Disabled, get amounts
        else{
            data.map( each => {
            description.push(each.description)
            amount.push(each.amount)
            bgColors.push(randomcolor())
            }) 
        }

        setState({
            description,
            amount,
            bgColors,
        })

    },[data])
   
    const breakDownData = {
        labels: state.description,
        datasets: [{
            data: state.amount,
            backgroundColor: state.bgColors
        }],
    }

    return(
        <>
            <Pie data={breakDownData} />
        </>
    )

}