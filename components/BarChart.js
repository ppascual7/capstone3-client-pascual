import {useState, useEffect, Fragment} from 'react'
import {Bar} from 'react-chartjs-2'
import moment from 'moment'

export default function BarChart({rawData}){

    const [months, setMonths] = useState(
        [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec",
        ]
    )
    const [amountPerMonth, setAmountPerMonth] = useState([])

    useEffect(()=>{

        // Sum total Amount per Month
        setAmountPerMonth(months.map(month => {

            let amount = 0

            rawData.forEach(element => {

                if(moment(element.date).format("MMM") === month){
                    amount += parseInt(element.amount)
                }
            })
            return amount
        }))
    }, [rawData])

    const data = {
        labels: months,
        datasets: [{
            label: "Monthly",
            backgroundColor: 'lightBlue',
            borderColor: 'white',
            borderWidth: 1,
            hoverBackgroundColor: 'orange',
            hoverBorderColor: "black",
            data: amountPerMonth
        }]
    }

    const options = 
    {
        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true
                    }
                }
            ]
        }
    }

    return (
        <>
        <div className="chart-container ml-10">
            <Bar data={data} options={options} />
        </div>
           
        </>
        
    )
}