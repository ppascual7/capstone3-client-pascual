import UserContext from './../UserContext';
import React, {useContext, useState, useEffect} from 'react'
import Aos from 'aos'
import "aos/dist/aos.css"

export default function NavBar(){

    const {user} = useContext(UserContext)
    const [state, setState] = useState(false)

    useEffect(()=>{

        Aos.init({duration: 500})
    
    },[])

    return (
        <nav class="navbar is-transparent">
            <div class="navbar-brand">
                <a href="/" class="navbar-item">
                    <p id="general-title" class="title is-4 ml-5 tag is-black">current</p>
                </a>
                <div class="navbar-burger" data-target="navbarExampleTransparentExample" onClick={(e) => setState(!state)}>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>

            {
                state && 
                <div data-aos="fade-in" class="mobile-menu">
                    <div class="dropdown is-active">
                        <div class="dropdown-trigger">
                        </div>
                        <div class="dropdown-menu dropdown-menu-border" id="dropdown-menu" role="menu">
                            {
                                (!user.id) ?
                                    <div class="dropdown-content dropdown-content-container">
                                        <a href="/" class="dropdown-item">
                                            About
                                        </a>
                                        <a href="/login" class="dropdown-item">
                                            Login
                                        </a>
                                        <a href="/signup" class="dropdown-item is-active">
                                            Signup
                                        </a>
                                    </div>
                                :
                                    <div class="dropdown-content dropdown-content-container">

                                        <a href="/record" class="dropdown-item">
                                            History
                                        </a>
                                        <a href="/category" class="dropdown-item">
                                            Category
                                        </a>
                                        <hr class="dropdown-divider"/>
                                        <a href="/breakdown" class="dropdown-item">
                                            Breakdown
                                        </a>
                                        <a href="/trend" class="dropdown-item">
                                            Trend
                                        </a>
                                        <a href="/monthlyExpense" class="dropdown-item">
                                            Monthly Expense
                                        </a>
                                        <a href="/monthlyIncome" class="dropdown-item">
                                            Monthly Income
                                        </a>
                                        <hr class="dropdown-divider"/>
                                        <a href="/logout" class="dropdown-item">
                                            Logout
                                        </a>
                                        <a href="#" class="dropdown-item">
                                            Profile
                                        </a>
                                    </div>
                            }
                    </div>
                </div>
                </div>
                
            }            

            <div id="navbarExampleTransparentExample" class="navbar-menu">
                {
                    (!user.id) 
                    ?
                    <>
                    <div class="navbar-start">
                            <a class="navbar-item" href="/">
                                About
                            </a>
                    </div>
                    <div class="navbar-end">
                        <div class="navbar-item">
                            <div class="field is-grouped">
                                <p class="control">
                                    <a class="bd-tw-button button" href="/login">
                                    <span>
                                        Login
                                    </span>
                                    </a>
                                </p>
                                <p class="control">
                                    <a class="button is-primary" href="/signup">
                                    <span>Sign up</span>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    </>
                    :
                    <>
                        <div class="navbar-start">
                            <a class="navbar-item" href="/">
                                About
                            </a>
                            <div class="navbar-item has-dropdown is-hoverable">
                                <a class="navbar-link" href="#">
                                    Records
                                </a>
                                <div class="navbar-dropdown is-boxed">
                                    <a class="navbar-item" href="/category">
                                        Categories
                                    </a>
                                    <a class="navbar-item" href="/record">
                                        History
                                    </a>
                                </div>
                            </div>
                            <div class="navbar-item has-dropdown is-hoverable">
                                <a class="navbar-link" href="#">
                                    Graphs
                                </a>
                                <div class="navbar-dropdown is-boxed">
                                    <a class="navbar-item" href="/monthlyIncome">
                                        Monthly Income
                                    </a>
                                    <a class="navbar-item" href="/monthlyExpense">
                                        Monthly Expense
                                    </a>
                                    <a class="navbar-item" href="/breakdown">
                                        Breakdown
                                    </a>
                                    <a class="navbar-item" href="/trend">
                                        Trend
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="navbar-end">
                            <div class="navbar-item has-dropdown is-hoverable mr-6">
                                <a class="navbar-link pr-6" href="#">
                                    {
                                        (user.profilePicture === undefined)
                                        ?
                                        <span>
                                            <img id="profilePicture" src='./images/placeholder.png'/>
                                        </span> 
                                        :
                                        <span>
                                            <img id="profilePicture" src={user.profilePicture} />
                                        </span> 
                                    }

                                    {localStorage.getItem("firstName")} 
                                </a>
                                <div class="navbar-dropdown is-boxed mr-6">
                                    <a class="navbar-item" href="/#">
                                        Profile
                                    </a>
                                    <a class="navbar-item" href="/logout">
                                        Logout
                                    </a>
                                    <hr class="navbar-divider"></hr>
                                    <a class="navbar-item" href="https://www.linkedin.com/in/patrick-gabriel-pascual" target="_blank">
                                        Contact Dev
                                    </a>
                                </div>
                            </div>
                        </div>
                    </>
                }
            </div>
                
        </nav>
    )
}