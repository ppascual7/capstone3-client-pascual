import { Line } from "react-chartjs-2";
import moment from "moment";
import React, {useEffect, useState} from "react";

export default function LineGraph({balance}) {

  const [balanceData, setBalanceData] = useState([]);

  useEffect(()=>{
    setBalanceData(balance.map( each => {
      return each.balance
    }))
  },[balance])

  const data = () => {
    return {
      backgroundColor: "lightBlue",
      labels: balanceData,
      datasets: [
        {
          label: "Balance Trend",
          data: balanceData,
          borderWidth: 3,
          fill: true,
          tension: 0.2,
          borderColor: "green"
        }
      ]
    };
  };

  return (
      <Line data={data} />
  );
}