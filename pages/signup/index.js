import { Form, Button } from 'react-bootstrap'
import React, { useEffect, useState } from 'react'
import Router from 'next/router'

export default function Register(){

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [isActive, setIsActive] = useState(false);
    const [loader, setLoader] = useState(false);
    const [status, setStatus] = useState("")

    const SERVER_URL = "https://stark-shelf-65651.herokuapp.com/"

    useEffect(()=>{

        if((firstName !=='' && lastName !== '' && email !== '' && password !== '' && confirmPassword !== '' && (password === confirmPassword))){

            setIsActive(true);
            
		} else {

            setIsActive(false);
            
        }
        
    },[firstName, lastName, email, password, confirmPassword])

    function registerUser(e){
        
        setLoader(true)

        e.preventDefault();

        fetch(`https://stark-shelf-65651.herokuapp.com/api/users/register`, {

            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName,
                lastName,
                email,
                password,
                confirmPassword
            })

        })
        .then( res => res.json())
        .then( result => {

            if(result === true) {

                setStatus("Successful Registration! ")

            }

            else if((result.error.message).includes("duplicate key error collection"))
                return setStatus("Email has already been taken")

            else{

                return setStatus("Something went wrong")

            }

        }).finally( () => {setLoader(false)})

        setFirstName("")
        setLastName("")
        setEmail("")
        setPassword("")
        setConfirmPassword("")
    }

    return(

        <div className="page-container">
                <div className="login-form-container">
                    <div className="title-container">
                        <p className="title is-1" id="general-title">Sign up</p>
    
                    </div>

                    <hr class="navbar-divider"/>

                    <form onSubmit={(e) => registerUser(e)}>
                        <input id="firstName" class="input mt-3 mb-1 is-primary" 
                            type="text" 
                            placeholder="First name"
                            value={firstName}
                            onChange={(e) => setFirstName(e.target.value)}
                            required
                        />
                        <input id="lastName" class="my-1 input is-primary" 
                            type="text" 
                            placeholder="Last Name"
                            value={lastName}
                            onChange={(e) => setLastName(e.target.value)}
                            required
                        />
                        <input id="email" class="my-1 input is-primary" 
                            type="email" 
                            placeholder="Email Address"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            required
                        />
                        <input id="password" class="my-1 input is-primary" 
                            type="password" 
                            placeholder="Password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                        />
                        <input id="confirmPassword" class="my-1 mb-6 input is-primary" 
                            type="password" 
                            placeholder="Confirm Password"
                            value={confirmPassword}
                            onChange={(e) => setConfirmPassword(e.target.value)}
                            required
                        />

                        <button disabled={!isActive} type="submit" class={`button is-danger is-fullwidth ${loader ? 'is-loading': ''}`}>Continue</button>
                    </form>
                    <div class="status-container">
                        {
                            (status.includes("Successful")) ?
                            <>
                                <p id="p-success" class="mt-3 is-6"> {status}</p>
                                <a href="/login"> Login </a>
                            </>
                            :
                            <p id="p-error" class="mt-3 p-error is-6"> {status}</p>
                        }
                    </div>
            
                </div>
            </div>
    )
}