import React, {useEffect} from 'react'
import Aos from 'aos'
import "aos/dist/aos.css"
import AnchorLink from 'react-anchor-link-smooth-scroll'

export default function Home() {

  useEffect(()=>{

    Aos.init({duration: 1500})

  },[])
  return (
      <>
          <div className="page-container">
              <div className="login-form-container">
                  <div className="page-container">
                      <img src="./images/current.png" />
                      <p data-aos="fade-in" className="title is-1" id="general-title">Welcome to Current</p>
                      <p className="title is-1" id="general-light">keep tabs of your finance</p>

                      <AnchorLink href="#seemore-page" class="button is-primary">See More</AnchorLink>
                  </div>
                      
              </div>
          </div>

          <div className="description-container">
                      <p data-aos="fade-up" className="title is-1" id="question-mark">?</p>
                      <p data-aos="fade-up" className="title is-1" id="general-title">What is current?</p>
                      <div class="seemore-page-subheading" data-aos="fade-up">
                        <p className="title is-1" id="general-light">is a money tracking app that will help manage your current spending, balances and budgets.</p>
                      </div>
          </div>
{/*            
          <div class="icon-container">
                            <p data-aos="fade-up" className="tag is-black title is-1" id="general-title">current</p>
           </div> */}
          
          <div id="seemore-page" className="seemore-page-container">
                    <p data-aos="fade-up" className="tag is-black title is-1" id="general-title">current</p>
                      <p data-aos="fade-up" className="title is-1" id="general-title">Key Features</p>

                        <div class="columns columns-container test">
                                <div class="column is-flex is-flex-direction-column is-align-items-center is-justify-content-center">
                                    <p data-aos="fade-up" className="title is-1" id="features-subtitle">Track your cash flow with ease</p>
                                    <div class="image-container">
                                        <figure data-aos="fade-in" class="image is-256x256 is-fullwidth">
                                            <img src="./images/coins.png"/>
                                        </figure>
                                    </div>
                                </div>
                                <div class="column is-flex is-flex-direction-column is-align-items-center is-justify-content-center">
                                    <p data-aos="fade-up" className="title is-1" id="features-subtitle">Real time data visualization</p>
                                    <div class="image-container is-flex is-align-items-center">
                                        <figure data-aos="fade-in" class="image is-256x256 is-fullwidth">
                                            <img class="mt-10" src="./images/piechart.png"/>
                                        </figure>
                                    </div>
                                </div>
                                <div class="column is-flex is-flex-direction-column is-align-items-center is-justify-content-center">
                                    <p data-aos="fade-up" className="title is-1" id="features-subtitle">Google integration</p>
                                    <div class="image-container">
                                        <figure data-aos="fade-in" class="image is-256x256 is-fullwidth">
                                            <img src="./images/google-logo.png"/>
                                        </figure>
                                    </div>
                                </div>
                        </div>
          </div>

          <div className="continue">
                <p data-aos="fade-up" className="title is-1" id="general-title"><a href="/login" class="button is-dark is-rounded">Continue with current</a></p>
          </div>
      </>
  )
}
