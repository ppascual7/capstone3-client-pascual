import '../styles/globals.css'
import 'bulma/css/bulma.css'
import { UserProvider } from '../UserContext';
import NavBar from './../components/NavBar'
import React, {useState, useEffect} from 'react'

function MyApp({ Component, pageProps }) {

  const [user, setUser] = useState({
    id: null,
    firstName: null,
    lastName: null,
    profilePicture: null

  })

  const unsetUser = () => {
    localStorage.clear();
    setUser({
      id: null,
      firstName: null,
      lastName: null,
      profilePicture: null
    })
  }

  useEffect(() => {

    setUser({
      id: localStorage.getItem('id'),
      firstName: localStorage.getItem('firstName'),
      lastName: localStorage.getItem('lastName'),
      profilePicture: localStorage.getItem('profilePicture')
    })

  }, [])

  return (
    <React.Fragment>
      <UserProvider value={{user, setUser, unsetUser}}>
        <NavBar />
            <Component {...pageProps} />
      </UserProvider>
    </React.Fragment>
  )
}

export default MyApp
