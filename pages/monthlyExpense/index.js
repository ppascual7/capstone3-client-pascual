import BarChart from './../../components/BarChart'
import React, {useEffect, useState} from 'react'
import moment from 'moment'

export default function MonthlyExpense(){

    const [categoryRecords, setCategoryRecords] = useState({"expense": [],"income": []})
    const [categories, setCategories] = useState([{key: "expense", value: "expenseEntries"}])

    const SERVER_URL = "https://stark-shelf-65651.herokuapp.com/"

    useEffect(()=>{
        fetch(`https://stark-shelf-65651.herokuapp.com/api/users/records`, {
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(result => {
            setCategoryRecords(result)
        })
    },[])

    let recordDetails = []
    categories.map(category => {
        return categoryRecords[category.key].map(categoryRecord => {
            return categoryRecord[category.value].map(eachRecord => {
                recordDetails.push(
                    {
                     description: eachRecord.description,
                     categoryName: categoryRecord.categoryName,
                     categoryType: eachRecord.key,
                     amount: eachRecord.amount,
                     date: eachRecord.dateCreated
                    }
                )
            })
        })
    })

    return (
        <>
                <div class="is-flex is-justify-content-center">
                    <p id="general-title" className="title is-1 pt-5">Monthly Expense </p>
                </div>
                <BarChart rawData={recordDetails}/>
        </>
    )
}