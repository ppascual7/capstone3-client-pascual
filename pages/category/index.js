import React, {useState, useEffect} from 'react'
import {Table, Button} from 'react-bootstrap'
import Aos from 'aos'
import "aos/dist/aos.css"

export default function Category(){

    const [allExpenseCategories, setAllExpenseCategories] = useState({expense: []})
    const [allIncomeCategories, setAllIncomeCategories] = useState({income: []})
    const [state, setState] = useState("")

    const SERVER_URL = "https://stark-shelf-65651.herokuapp.com/"

    useEffect(()=>{

        Aos.init({duration: 1000})        

       fetch("https://stark-shelf-65651.herokuapp.com/api/users/getExpenseCategories", {
            method: "POST",
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({_id: localStorage.getItem('id')})
        })
        .then(res => res.json())
        .then(result => {
            setAllExpenseCategories(result)
        })

        fetch(`${SERVER_URL}api/users/getIncomeCategories`, {
            method: "POST",
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({_id: localStorage.getItem('id')})
        })
        .then(res => res.json())
        .then(result => {
            setAllIncomeCategories(result)
        })
    },[state])

    function deleteCategory(e, categoryId, categoryType){
        e.preventDefault();
        let url = `https://stark-shelf-65651.herokuapp.com/api/users/removeExpenseCategory`
        if(categoryType === "income") url = `https://stark-shelf-65651.herokuapp.com/api/users/removeIncomeCategory`
        fetch(url, {
            method: "DELETE",
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({categoryId})
        })
        .then(res => res.json())
        .then(result => {
            if(result === true) return setState(`category ${categoryId} deleted`)
            return alert("Something went wrong, category not deleted")
        })
    }

    const expenseCategories = allExpenseCategories.expense.map(expenseCategory => {
        return (
            <tr data-aos="fade-up" key={expenseCategory._id}>
                <td>{expenseCategory.categoryName}</td>
                <td>Expense</td>
                <td>
                    <button type="button" class="button is-danger is-small"
                    onClick={(e) => deleteCategory(e, expenseCategory._id, "expense")}
                    > 
                    x 
                    </button>
                </td>
            </tr>
        )
    })

    const incomeCategories = allIncomeCategories.income.map(incomeCategory => {
        return (
            <tr data-aos="fade-up" key={incomeCategory._id}>
                <td>{incomeCategory.categoryName}</td>
                <td>Income</td>
                <td>
                    <button type="button" class="button is-danger is-small"
                    onClick={(e) => deleteCategory(e, incomeCategory._id, "income")}
                    > 
                    x 
                    </button>
                </td>
            </tr>
        )

    })

    return(
        <>
            <div>
                <div class="categories-page-container">
                    <div class="columns columns-header-category">
                        <div class="column is-flex is-align-items-flex-end"> 
                            <a type="submit" class="is-primary button" href="category/new"> 
                                Add 
                            </a>
                        </div>
                        <div class="column is-flex is-justify-content-center mt-5"> 
                            <p id="general-subtitle" className="title is-1 pt-5">
                                <span id="general-subtitle" class="tag is-dark">current</span>categories
                            </p>
                        </div>
                        <div class="column"> </div>
                    </div>
                   
                    
                    <table data-aos="fade-up" class="table-container table is-hoverable">
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Type</th>
                                    <th className="w-25">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {expenseCategories}
                                {incomeCategories}
                            </tbody>
                    </table>
                </div>
            </div>
            
        </>
    )

}