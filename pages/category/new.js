import React, {useState, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import UserContext from '../../UserContext'

export default function NewCategory(){

    const {user} = useContext(UserContext)

    const [categoryName, setCategoryName] = useState("")
    const [categoryType, setCategoryType] = useState("Income")
    const [loader, setLoader] = useState(false)
    const [status, setStatus] = useState("")

    const SERVER_URL = "https://stark-shelf-65651.herokuapp.com/"

    function addCategory(e){

        setLoader(true)

        e.preventDefault();

        let url=`https://stark-shelf-65651.herokuapp.com/api/users/addIncomeCategory`

        if(categoryType === "Expense") url = `https://stark-shelf-65651.herokuapp.com/api/users/addExpenseCategory`

        fetch(url, {
            method: "POST",
            headers: {
                "Content-type": 'application/json',
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                _id: user.id,
                categoryName
            })
        })
        .then(res => res.json())
        .then( result => {
            if(result === true) return setStatus(`Category ${categoryName} successfully added to ${categoryType}`)
            if(result.error.message.includes("exists")) return setStatus(`Category ${categoryName} already exists in ${categoryType}`)
            else return setStatus(`Something went wrong`)
        })
        .finally(() => setLoader(false))
    }

    return(
        <div className="page-container">
                <div className="login-form-container">
                    <div className="title-container">
                        <p className="title is-1" id="general-title">Add Category</p>
    
                    </div>

                    <hr class="navbar-divider"/>

                    <form onSubmit={(e) => addCategory(e)}>
                        <div class="select is-fullwidth mb-3">
                            <select id="categoryType" class="select my-3 input is-primary"
                                onChange={(e) => setCategoryType(e.target.value)}>
                                <option>Income</option>
                                <option>Expense</option>
                            </select>
                        </div>
                        <input id="categoryName" class="my-3 input is-primary" 
                            type="text" 
                            placeholder="Category Name"
                            value={categoryName}
                            onChange={(e) => setCategoryName(e.target.value)}
                            required
                        />
                        <button type="submit" class="button is-primary is-fullwidth">Add</button>
                    </form>
                    <a href="/category" class="mt-2 button is-info is-fullwidth">View Categories</a>
                    <div class="status-container">
                    {
                            (status.includes("successfully")) ?
                            <>
                                <p id="p-success" className="mt-3 is-6"> {status}</p>
                            </>
                            :
                                <p id="p-errpr" className="mt-3 is-6"> {status}</p>
                    }
                    </div>
                </div>
            </div>
    )

}