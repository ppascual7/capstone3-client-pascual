import Line from './../../components/LineGraph'
import { useEffect, useState } from 'react'
import moment from 'moment'
import { Form, Container, Row, Col } from 'react-bootstrap'
import dateFormatter from './../../helpers/dateFormatter'

export default function Trend(){

    const [categoryRecords, setCategoryRecords] = useState({"expense": [],"income": []})
    const [categories, setCategories] = useState([{key: "income", value: "incomeEntries"},{key: "expense", value: "expenseEntries"}])

    let currentDate = new Date();
    let dateToday = dateFormatter((currentDate.getFullYear()+'-'+(currentDate.getMonth()+1)+'-'+(currentDate.getDate())),"YYYY-MM-DD")
    let dateYesterday = dateFormatter((currentDate.getFullYear()+'-'+(currentDate.getMonth()+1)+'-'+(currentDate.getDate()-1)),"YYYY-MM-DD")

    const [fromDate, setFromDate] = useState(dateYesterday)
    const [toDate, setToDate] = useState(dateToday)

    useEffect(()=>{
        fetch(`https://stark-shelf-65651.herokuapp.com/api/users/records`, {
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(result => {
            setCategoryRecords(result)
        })
    },[])

    let recordDetails = []
    categories.map(category => {
        return categoryRecords[category.key].map(categoryRecord => {
            return categoryRecord[category.value].map(eachRecord => {
                if(category.key === "expense") eachRecord.amount=(eachRecord.amount*-1)
                recordDetails.push(
                    {
                     description: eachRecord.description,
                     categoryName: categoryRecord.categoryName,
                     categoryType: category.key,
                     amount: eachRecord.amount,
                     date: eachRecord.dateCreated
                    }
                )
            })
        })
    })

    recordDetails = recordDetails.sort(function(a,b){
        return new Date(dateFormatter(a.date, "YYYYMMDD") - dateFormatter(b.date, "YYYYMMDD"))
    })

    let recordDetailsByDate = recordDetails.filter(record => {
        return dateFormatter(record.date, "YYYYMMDD") >= dateFormatter(fromDate, "YYYYMMDD") &&
        dateFormatter(record.date, "YYYYMMDD") <= dateFormatter(toDate, "YYYYMMDD")
    })

    let balance = 0
    recordDetails = recordDetailsByDate.map(element => {
        return {balance: balance+=element.amount, date: element.date}
    })

    console.log(recordDetails)

return(

    
    <>

<>
            <div class="page-container">
                <div class="columns columns-container">
                    <div class="column is-flex is-flex-direction-column is-justify-content-flex-end calendar-dimensions pt-10px">
                        <p>From</p>
                        <input id="fromDate" type="date"
                            value={fromDate}
                            onChange={(e) => setFromDate(e.target.value)}
                        ></input>
                    </div>
                    <div class="column is-flex is-flex-direction-column is-justify-content-flex-end calendar-dimensions pt-10px">
                        <p>To</p>
                        <input id="toDate" type="date"
                                value={toDate}
                                onChange={(e) => setToDate(e.target.value)}
                        ></input>
                    </div>
                </div>
                <div class="is-flex is-justify-content-center">
                    <p id="general-title" className="title is-1">Balance Trend </p>
                </div>
                <div class="line-container">
                    <Line balance={recordDetails}/>
                </div>
            </div>
        </>

    </>
)
}