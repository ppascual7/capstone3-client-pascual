import { Form, Button } from 'react-bootstrap';
import React, { useState, useEffect,useContext } from 'react';
import UserContext from '../../UserContext';
import Router from 'next/router';
import { GoogleLogin } from 'react-google-login';

export default function Login(){

    const {user,setUser} = useContext(UserContext)

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);
    const [loader, setLoader] = useState(false);
    const [status, setStatus] = useState("")

    useEffect(() => {

        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email, password, loader]);
    

    function authenticate(e) {
        
        setLoader(true)

        e.preventDefault();

        fetch(`https://stark-shelf-65651.herokuapp.com/api/users/login`, {
            method: "POST",
            headers: {
                'Content-Type': "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {

            localStorage.setItem('token', data.accessToken)

            if(data.accessToken){
                fetch(`https://stark-shelf-65651.herokuapp.com/api/users/details`,{

                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    }
                })
                .then(res => res.json())
                .then(data => {

                    localStorage.setItem('id',data._id)
                    localStorage.setItem('firstName', data.firstName)
                    setUser({

                        id: data._id,
                        firstName: data.firstName,
                        lastName: data.lastName

                    })
                })

                Router.push('/')
            }
            
            else {
                setStatus("Invalid Credentials")
            }

            setEmail('');
            setPassword('');
        })
        .finally(() => setLoader(false))
    }

    function authenticateGoogleToken(response){

        setLoader(true)

        localStorage.setItem("profilePicture",response.profileObj.imageUrl)

        fetch(`https://stark-shelf-65651.herokuapp.com/api/users/verify-google-id-token`, {

            method: "POST",
            headers: 
            {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
            tokenId: response.tokenId
            })

        })
        .then(res => res.json())
        .then(data => {
            
            if (typeof data.accessToken !== 'undefined'){

                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)

            } else {

                if(data.error === 'google=auth-error'){
                    setStatus('Google Authentication procedure failed',)
                } else if (data.error == 'login-type-error'){
                    setStatus('You may have registered through a different login procedure')
                }
            }
        }).finally(() => setLoader(false))
    }

    function retrieveUserDetails(accessToken) {

        fetch(`https://stark-shelf-65651.herokuapp.com/api/users/details`, {

            headers: {
                "Authorization": `Bearer ${accessToken}`
            }
        })
        .then(res => res.json())
        .then(data => {

            Router.push('/')
            localStorage.setItem('id',data._id)
            localStorage.setItem('firstName', data.firstName)

            setUser({
                        id: data._id,
                        firstName: data.firstName,
                        lastName: data.lastName,
                        profilePicture: localStorage.getItem('profilePicture')
            })
        })
    }

    return (
        <>
            <div className="page-container">
                <div className="login-form-container">
                    <div className="title-container">
                        <p className="title is-1" id="general-title">Log in</p>
    
                    </div>

                    <GoogleLogin
                        clientId="140527931092-ghc366f97h0it0q6v8b2vqt1nevnv8q2.apps.googleusercontent.com"
                        buttonText="Continue with Google"
                        onSuccess={authenticateGoogleToken}
                        onFailure={authenticateGoogleToken}
                        cookiePolicy={'single_host_origin'}
                        className="text-center my-2 d-flex justify-content-center"
                    />

                    <hr class="navbar-divider"/>

                    <form onSubmit={(e) => authenticate(e)}>
                        <input id="userEmail" class="input mt-3 is-primary" 
                            type="email" 
                            placeholder="Enter your email address.."
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            required
                        />
                        <input id="password" class="my-3 input is-primary" 
                            type="password" 
                            placeholder="Password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                        />
                        <button disabled={!isActive} type="submit" class={`button is-danger is-fullwidth ${loader ? 'is-loading': ''}`}>Log in</button>
                    </form>
                    <div class="status-container">
                        <p class="mt-3 p-error is-6"> {status}</p>
                    </div>
                    <p class="mt-3 general-p is-6"> Don't have an account yet? <a href="/signup">Sign up here</a></p>
                </div>
            </div>
        </>
    )
}