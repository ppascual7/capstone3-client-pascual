import PieChart from './../../components/PieChart'
import React, { useEffect, useState } from 'react'
import { Form, Row, Col } from 'react-bootstrap'
import dateFormatter from './../../helpers/dateFormatter'

export default function Breakdown(){

    const [categoryRecords, setCategoryRecords] = useState({"expense": [],"income": []})
    const [categories, setCategories] = useState([{key: "income", value: "incomeEntries"},{key: "expense", value: "expenseEntries"}])
    const [incomeOverExpenseFlag, setIncomeOverExpenseFlag] = useState(false)

    const SERVER_URL = "https://stark-shelf-65651.herokuapp.com/"

    let currentDate = new Date();
    let dateToday = dateFormatter((currentDate.getFullYear()+'-'+(currentDate.getMonth()+1)+'-'+(currentDate.getDate())),"YYYY-MM-DD")
    let dateYesterday = dateFormatter((currentDate.getFullYear()+'-'+(currentDate.getMonth()+1)+'-'+(currentDate.getDate()-1)),"YYYY-MM-DD")

    const [fromDate, setFromDate] = useState(dateYesterday)
    const [toDate, setToDate] = useState(dateToday)

    useEffect(()=>{
        fetch(`https://stark-shelf-65651.herokuapp.com/api/users/records`, {
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(result => {
            setCategoryRecords(result)
        })
    },[])
    
    let recordDetails = []
    categories.map(category => {
        return categoryRecords[category.key].map(categoryRecord => {
            return categoryRecord[category.value].map(eachRecord => {
                recordDetails.push(
                    {
                     description: eachRecord.description,
                     categoryName: categoryRecord.categoryName,
                     categoryType: category.key,
                     amount: eachRecord.amount,
                     date: eachRecord.dateCreated
                    }
                )
            })
        })
    })

    let recordDetailsByDate = recordDetails.filter(record => {
        return dateFormatter(record.date, "YYYYMMDD") >= dateFormatter(fromDate, "YYYYMMDD") &&
        dateFormatter(record.date, "YYYYMMDD") <= dateFormatter(toDate, "YYYYMMDD")
    })

    return(
        <>
            <div class="page-container">
                <div class="columns columns-container">
                    <div class="column is-flex is-flex-direction-column is-justify-content-flex-end calendar-dimensions pt-10px">
                        <p>From</p>
                        <input id="fromDate" type="date"
                            value={fromDate}
                            onChange={(e) => setFromDate(e.target.value)}
                        ></input>
                    </div>
                    <div class="column is-flex is-flex-direction-column is-justify-content-flex-end calendar-dimensions pt-10px">
                        <p>To</p>
                        <input id="toDate" type="date"
                                value={toDate}
                                onChange={(e) => setToDate(e.target.value)}
                        ></input>
                    </div>
                    <div class="column is-flex is-justify-content-center">
                        <div class="select is-fullwidth mt-5">
                                <select id="categoryType" class="select input is-primary" 
                                    onChange={(e) => {
                                        if(e.target.value === "Income") setCategories([{key: "income", value: "incomeEntries"}])
                                        if(e.target.value === "Expense") setCategories([{key: "expense", value: "expenseEntries"}])
                                        if(e.target.value === "All") setCategories([{key: "income", value: "incomeEntries"},{key: "expense", value: "expenseEntries"}])
                                        if(e.target.value === "Income/Expense") {
                                            setCategories([{key: "income", value: "incomeEntries"},{key: "expense", value: "expenseEntries"}])
                                            setIncomeOverExpenseFlag(true)
                                        } else {
                                            setIncomeOverExpenseFlag(false)
                                        }}}>
                                    <option>All</option>
                                    <option>Income</option>
                                    <option>Expense</option>
                                    <option>Income/Expense</option>
                                </select>
                        </div>

                        
                    </div>
                </div>
                <div class="is-flex is-justify-content-center">
                    <p id="general-title" className="title is-1">Breakdown </p>
                </div>
                <div className="pie-container">
                    <PieChart data={recordDetailsByDate} incomeOverExpense={incomeOverExpenseFlag}/>
                </div>
            </div>
        </>
    )

}