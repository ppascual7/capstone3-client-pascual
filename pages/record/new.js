import { Card, Button, Form } from 'react-bootstrap'
import React, {useState, useEffect} from 'react'
import { urlObjectKeys } from 'next/dist/next-server/lib/utils'
import dateFormatter from './../../helpers/dateFormatter'

export default function newRecord(){

    const [categoryType, setCategoryType] = useState("Income")
    const [categoryName, setCategoryName] = useState([])
    const [selectCategory, setSelectCategory] = useState("")
    const [amount, setAmount] = useState("0")
    const [description, setDescription] = useState("")
    const [status, setStatus] = useState("")
    
    let currentDate = new Date();    
    let dateToday = (currentDate.getFullYear()+'-'+(currentDate.getMonth()+1)+'-'+(currentDate.getDate()))
    

    dateFormatter(dateToday,"YYYY-MM-DD")+"T"+currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds()
    // console.log("22", dateToday2)
    // console.log("2", dateFormatter(dateToday2,"YYYY-MM-DD-THH:MM:SSS"))

    const SERVER_URL = "https://stark-shelf-65651.herokuapp.com/"

    useEffect(()=>{

        let url = `https://stark-shelf-65651.herokuapp.com/api/users/getIncomeCategories`
        let type = "income"

        if(categoryType === "Expense") {
            url = `https://stark-shelf-65651.herokuapp.com/api/users/getExpenseCategories`
            type = "expense"
        }

        fetch(url, {
            method: "POST",
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({_id: localStorage.getItem('id')})
        })
        .then(res => res.json())
        .then(result => {
            setCategoryName(result[type])
            if(result[type].length !== 0){
                setSelectCategory(result[type][0].categoryName)
            }
        })

    },[categoryType])

    const categoryOptions = categoryName.map(category => {
        return (
            <option key={category._id}>{category.categoryName}</option>  
        )
    })

    function addRecord(e){
        e.preventDefault()
        
        let categoryProps = categoryOptions.find(function (element){
            return (Object.values(element.props)[0] === selectCategory)
        })

        let url = `https://stark-shelf-65651.herokuapp.com/api/users/addIncomeEntry`

        if(categoryType === "Expense") {
            url = `https://stark-shelf-65651.herokuapp.com/api/users/addExpenseEntry`
        }

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                categoryId: categoryProps.key,
                amount,
                description,
                dateCreated: dateFormatter(dateToday,"YYYY-MM-DD")+"T"+currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds()
            })
        })
        .then(res=>res.json())
        .then(data => {
            if(data === true) return setStatus("Entry successfully added")
            setStatus("Something went wrong")
        }).finally(() => {
            setAmount("0")
            setDescription("")
        })
    }

    return (
        <>
        
            <div className="page-container">
                <div className="login-form-container">
                    <div className="title-container">
                        <p className="title is-1" id="general-title">Add Record</p>
    
                    </div>

                    <hr class="navbar-divider"/>

                    <form onSubmit={(e) => addRecord(e)}>
                        <div class="select is-fullwidth mb-3">
                            <select id="categoryType" class="select my-3 input is-primary"
                                onChange={(e) => setCategoryType(e.target.value)}>
                                <option>Income</option>
                                <option>Expense</option>
                            </select>
                        </div>
                        <div class="select is-fullwidth mb-3">
                            <select id="categoryName" class="my-3 input is-primary" 
                                onChange={(e) => setSelectCategory(e.target.value)}>
                                {categoryOptions}
                            </select>
                        </div>

                        <input id="amount" class="my-3 input is-primary" 
                            type="number" 
                            placeholder="Amount"
                            value={amount}
                            onChange={(e) => setAmount(e.target.value)}
                            required
                        />
                        <input id="description" class="mb-3 input is-primary" 
                            type="test" 
                            placeholder="Description"
                            value={description}
                            onChange={(e) => setDescription(e.target.value)}
                            required
                        />
                        <button type="submit" class="button is-primary is-fullwidth">Add</button>
                    </form>
                    <a href="/record" class="mt-2 button is-info is-fullwidth">View Records</a>
                    <div class="status-container">
                    {
                            (status.includes("successfully")) ?
                            <>
                                <p id="p-success" className="mt-3 is-6"> {status}</p>
                            </>
                            :
                                <p id="p-errpr" className="mt-3 is-6"> {status}</p>
                    }
                    </div>
                </div>
            </div>
        </>
    )

}