import React, {useEffect, useState} from 'react'
import {Card, Row, Col, Button, InputGroup, Form, } from 'react-bootstrap'
import Link from 'next/link'
import moment from 'moment'
import currencyFormatter from './../../helpers/currencyFormatter'
import dateFormatter from './../../helpers/dateFormatter'
import Aos from 'aos'
import "aos/dist/aos.css"

export default function Record(){

    const [categoryRecords, setCategoryRecords] = useState({"expense": [],"income": []})
    const [categories, setCategories] = useState([{key: "income", value: "incomeEntries"},{key: "expense", value: "expenseEntries"}])
    const [searchKey, setSearchKey] = useState("")
    const [state, setState] = useState("")

    const SERVER_URL = "https://stark-shelf-65651.herokuapp.com/"

    useEffect(() => {

        Aos.init({duration: 1000})   

        fetch(`https://stark-shelf-65651.herokuapp.com/api/users/records`, {
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(result => {
            setCategoryRecords(result)
        })

    },[state, categories])

    //Get income and expense record history from fetch result. Return array of needed properties.
    let recordDetails = []
    categories.map(category => {
        return categoryRecords[category.key].map(categoryRecord => {
            return categoryRecord[category.value].map(eachRecord => {
                console.log("Date", eachRecord.dateCreated)
                if(category.key === "expense") eachRecord.amount=(eachRecord.amount*-1)
                recordDetails.push(
                    {_id: eachRecord._id,
                     description: eachRecord.description,
                     date: moment(eachRecord.dateCreated).format("MMM DD YYYY h:mm:ss A"),
                     categoryName: categoryRecord.categoryName,
                     categoryType: category.key,
                     amount: eachRecord.amount,
                })
            })
        })
    })

    // Sort from oldest to latest
    recordDetails = recordDetails.sort(function(a,b){
        return new Date(dateFormatter(a.date,"YYYYMMDD") - dateFormatter(b.date,"YYYYMMDD"))
    })
    
    
    // Sort from search keyword
    let filteredRecords = recordDetails.filter(element => {
         return element.description.toLowerCase().startsWith(searchKey.toLowerCase())
    })

    function deleteEntry(e, entryId, entryType){

        e.preventDefault();
        let url = (`https://stark-shelf-65651.herokuapp.com/api/users/removeExpenseEntry`)

        if(entryType === "income") url = (`${SERVER_URL}/api/users/removeIncomeEntry`)
        
        fetch(url, {
            method: "DELETE",
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({entryId})
        })
        .then(res => res.json())
        .then(result => {
            if(result === true) return setState(`entry ${entryId} deleted`)
            return alert("Something went wrong, category not deleted")
        })
    }

    let balance = 0
    let records = filteredRecords.map(element => {

        return(
            <>
            <div data-aos="fade-up" class="card mb-3" id={element._id}>
                <div class="card-content">
                    <div class="columns">
                        <div class="column">
                            <div class="columns is-mobile">
                                <div class="column">
                                    <p id="card-heading-p">{element.description}</p>
                                </div>
                                <div class="column">
                                    <div class="is-flex is-align-items-flex-end is-flex-direction-column">
                                        <button class="delete"
                                            onClick={(e) => deleteEntry(e, element._id, element.categoryType)}>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="columns is-mobile card-column-footer">
                                <div class="column">
                                    <p id="card-subtext-p">{element.categoryName}</p>
                                    <p id="card-subtext-p">{element.date}</p>
                                </div>
                                <div class="column">
                                    <div class="is-flex is-align-items-flex-end is-flex-direction-column">
                                    {
                                        (element.categoryType === "income") ? 
                                        <p class="tag is-success card-tag">
                                            {element.amount}
                                        </p>  
                                        :
                                        <p class="tag is-danger card-tag">
                                            {element.amount}
                                        </p>  
                                    }
                                    {
                                        (searchKey === "") ?
                                        
                                            (categories.length > 1)  ?
                                            <>
                                                <p id="card-subtext-p">Balance: {currencyFormatter.format(balance+=element.amount)}</p>
                                            </>
                                            :
                                            <>
                                                <p id="card-subtext-p">Total: {currencyFormatter.format(balance+=element.amount)}</p>
                                            </>
                                        :
                                        null
                                    }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </>
        )
    })

    return(

            <div>
                <div class="categories-page-container">
                    <div class="columns columns-header-category">
                        <div class="column is-flex is-align-items-flex-end"> 
                            <a type="submit" class="is-primary button" href="record/new"> 
                                Add 
                            </a>
                        </div>
                        <div class="column is-flex is-justify-content-center mt-5"> 
                            <p id="general-subtitle" className="title is-1 pt-5">
                                <span id="general-subtitle" class="tag is-dark">current</span>record
                            </p>
                        </div>
                        <div class="column"> </div>
                    </div>
                    <div class="columns is-full columns-container">
                        <div class="column">
                            <input class="input is-primary" type="text" placeholder="Search"
                                onChange={(e) => setSearchKey(e.target.value)}>
                            </input>
                        </div>
                        <div class="column">
                            <div class="select is-fullwidth">
                                <select id="categoryType" class="select input is-primary" onChange={e => {
                                    if(e.target.value === "Income") setCategories([{key: "income", value: "incomeEntries"}])
                                    if(e.target.value === "Expense") setCategories([{key: "expense", value: "expenseEntries"}])
                                    if(e.target.value === "All") setCategories([{key: "income", value: "incomeEntries"},{key: "expense", value: "expenseEntries"}])
                                }}>
                                    <option>Income</option>
                                    <option>Expense</option>
                                    <option>All</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-container">
                        {
                            (records.length === 0) ?
                            <p id="general-subtitle" className="title is-1 pt-5">No Records Found</p>
                            :
                            <>
                            { (categories.length > 1)  
                                ?
                                <p id="card-heading-p" class="mb-3"> Current Balance: <span> {currencyFormatter.format(balance)} </span></p>
                                :
                                <p id="card-heading-p" class="mb-3"> Total {categories[0].key}: <span> {currencyFormatter.format(balance)} </span></p>
                            }
                            {records}
                            </>
                        }   
                    </div>
                </div>
            </div>
    )
}